import React, { Component } from 'react';
import './App.css';
import Card from './Card/Card'

class App extends Component {

    state = {
        cards: [
            { id: "11a", name: "Ballard", age: 110 },
            { id: "12b", name: "Lodin", age: 200 },
            { id: "13c", name: "Ni-Li", age: 97 }
        ],
        showCards: false
    }

    nameChangedHandler = ( event, id ) => {

        const cardIndex = this.state.cards.findIndex( c => {
            return c.id === id;
        });

        // Create copy of element since it will be modified later
        const card = {
            ...this.state.cards[cardIndex]
        }

        card.name = event.target.value;
        const cards = [...this.state.cards];
        cards[cardIndex] = card;

        this.setState({cards: cards})
    }

    toggleCardsHandler = () => {
        const doesShow = this.state.showCards;
        this.setState({showCards: !doesShow});
    }

    deleteCardHandler = ( cardIndex ) => {
        //const newCards = this.state.cards.slice();
        const newCards = [...this.state.cards];
        newCards.splice(cardIndex, 1);
        this.setState({cards:newCards});
    }

    render() {

        let cards = null;
        let buttonText = 'Show Cards';

        if ( this.state.showCards ) {

            cards = (
                <div className="wrapper">
                    {this.state.cards.map( (card, index) => {
                        return <Card
                            click={this.deleteCardHandler.bind(this, index)}
                            name={card.name}
                            age={card.age}
                            key={card.id}
                            changed={ (event) => this.nameChangedHandler(event, card.id)} />
                    })}
                </div>
            )
            buttonText = "Hide Cards";
        }

        return (
            <div className="App">
                <h1>Chicago by Night</h1>
                <button className="btn" onClick={this.toggleCardsHandler}>{buttonText}</button>
                {cards}
            </div>
        );
    }
}

export default App;
