import React from 'react';
import './Card.css';

const card = ( props ) => {
    return (
        <div className="card">
            <p className="card__intro" onClick={props.click}>I'm {props.name}! I've been a vampire for {props.age}</p>
            <input className="card__input" onChange={props.changed} placeholder="Type a Name"/>
        </div>
    )
}

export default card;
